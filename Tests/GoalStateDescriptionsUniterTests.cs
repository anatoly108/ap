﻿using System.Collections.Generic;
using AP.Core.Agent;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Core.Tests.TestAgent.Actions;
using Core.Tests.TestAgent.Goals.Goal1;
using Core.Tests.TestAgent.Goals.Goal2;
using Core.Tests.TestAgent.Goals.Goal3;
using Core.Tests.TestAgent.Goals.Goal4;
using Core.Tests.TestAgent.State;

namespace Core.Tests
{
    [TestClass]
    public class GoalStateDescriptionsUniterTests
    {
        [TestMethod]
        public void Unite_Test()
        {
            var descriptionToStateConverter = new TestDescriptionToStateConverter();
            var goals = new List<Goal>
                {
                    new Goal1(new Goal1Condition(descriptionToStateConverter),
                        new Goal1Target(descriptionToStateConverter), Priority.A),
                    new Goal2(new Goal2Condition(descriptionToStateConverter), 
                        new Goal2Target(descriptionToStateConverter), Priority.C),
                    new Goal3(new Goal3Condition(descriptionToStateConverter),
                        new Goal3Target(descriptionToStateConverter), Priority.D),
                    new Goal4(new Goal4Condition(descriptionToStateConverter),
                        new Goal4Target(descriptionToStateConverter), Priority.C)
                };
            var target = new GoalUniter();

            var expectedState = new TestState(10, 20, 20, null);
            var expectedDescription = expectedState.ToDescription();
            
            var currentState = new TestState(null, null, null, null);
            var currentDescription = currentState.ToDescription();
            var actual = target.Unite(goals, currentDescription);
            Assert.AreEqual(expectedDescription, actual);
        }

        [TestMethod]
        public void CheckForConflict_Test()
        {
            var descriptionToStateConverter = new TestDescriptionToStateConverter();
            var goal1 = new Goal1(new Goal1Condition(descriptionToStateConverter),
                new Goal1Target(descriptionToStateConverter), Priority.A);
            var goal2 = new Goal2(new Goal2Condition(descriptionToStateConverter),
                new Goal2Target(descriptionToStateConverter), Priority.C);

            var currentState = new TestState(null, null, null, null);
            var currentDescription = currentState.ToDescription();

            var target = new GoalUniter();
            const bool expected = false;
            var actual = target.CheckForConflict(goal1, goal2, currentDescription);

            Assert.AreEqual(expected, actual);
        }
    }
}
