﻿using AP.Core.Agent.StateTools.Extension;
using Core.Tests.TestAgent.State;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Core.Tests
{
    [TestClass]
    public class DescriptionComparerExtensionsTests
    {
        [TestMethod]
        public void DescriptionComparerExtensions_Satisfaction_Test1()
        {
            var state = new TestState(10, 10, 10, 10).ToDescription();
            var stateToCheck = new TestState(10, null, 10, null).ToDescription();

            Assert.IsTrue(state.Satisfaction(stateToCheck));
        }

        [TestMethod]
        public void DescriptionComparerExtensions_Satisfaction_Test2()
        {
            var state = new TestState(10, 10, 10, 10).ToDescription();
            var stateToCheck = new TestState(10, 30, 10, null).ToDescription();

            Assert.IsFalse(state.Satisfaction(stateToCheck));
        }
    }
}