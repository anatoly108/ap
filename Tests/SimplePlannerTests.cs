﻿using System.Collections.Generic;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.PlaningTools;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using Core.Tests.TestAgent.State;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Core.Tests
{
    [TestClass]
    public class SimplePlannerTests
    {
        [TestMethod]
        public void SimplePlanner_MakePlan_OneActionTest()
        {
            var initialState = new TestState(0, 0, 0, 0).ToDescription();
            var goalTargetState = new TestState(10, null, null, null).ToDescription();

            var goalCondition = MockRepository.GenerateStrictMock<IStateProvider>();
            goalCondition.Expect(x => x.Get(null)).IgnoreArguments().Return(null).Repeat.Any();

            var goalTarget = MockRepository.GenerateStrictMock<IStateProvider>();
            goalTarget.Expect(x => x.Get(null)).IgnoreArguments().Return(goalTargetState).Repeat.Any();

            var planner = new SimplePlanner();

            var action = MockRepository.GenerateStrictMock<IAction>();

            var actionConditionState = new TestState(0, null, null, null).ToDescription();
            var actionCondition = MockRepository.GenerateStrictMock<IStateProvider>();
            actionCondition.Expect(x => x.Get(null)).IgnoreArguments().Return(actionConditionState).Repeat.Any();

            var actionResultState = new TestState(10, null, null, null).ToDescription();
            var actionResult = MockRepository.GenerateStrictMock<IStateProvider>();
            actionResult.Expect(x => x.Get(null)).IgnoreArguments().Return(actionResultState).Repeat.Any();

            action.Expect(x => x.ActionConditionStateProvider).Return(actionCondition).Repeat.Any();
            action.Expect(x => x.ActionResultStateProvider).Return(actionResult).Repeat.Any();

            var actions = new[] {action};

            var expectedPlanList = new Queue<IAction>();
            expectedPlanList.Enqueue(action);
            var expected = new Plan(expectedPlanList);

            var actual = planner.TryPlan(initialState, goalTargetState, actions);

            CollectionAssert.AreEqual(expected.ActionsPlan, actual.ActionsPlan);
        }
    }
}