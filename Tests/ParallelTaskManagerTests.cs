﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Core.Tests
{
    [TestClass]
    public class ParallelTaskManagerTests
    {
        private delegate void Func(IState state);

        [TestMethod]
        public void ParallelTaskManager_NewTask_TestForAddingSameAction()
        {
            var longAction = MockRepository.GenerateStrictMock<IAction>();
            var ts = new CancellationTokenSource();
            var ct = ts.Token;
            Func longActionFunc = x =>
            {
                while (true)
                {
                    if (ct.IsCancellationRequested)
                    {
                        break;
                    }
                }
            };
            longAction.Stub(x => x.Handle(null)).IgnoreArguments().Do(longActionFunc);
            longAction.Expect(x => x.Name).Return("LongAction").Repeat.Any();

            var target = new ParallelTaskManager(new RunningActionsController());

            var task1 = target.NewTask(longAction, null, ct);
            var task2 = target.NewTask(longAction, null, ct);
            ts.Cancel();
            Assert.IsNull(task2);
        }
    }
}