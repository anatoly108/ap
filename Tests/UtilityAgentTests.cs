﻿using System.Threading;
using System.Threading.Tasks;
using AP.Core;
using AP.Core.Agent;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.Components;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.PerceptionTools;
using AP.Core.Agent.PlaningTools;
using AP.Core.Agent.SensorTools;
using Core.Tests.TestAgent;
using Core.Tests.TestAgent.Perception;
using Core.Tests.TestAgent.State;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Core.Tests
{
    [TestClass]
    public class UtilityAgentTests
    {
        [TestMethod]
        public void UtilityAgent_ForOneSimpleGoal_Test()
        {
            var initialState = new TestState(0, 0, 0, 0);
            var initialPerception = new TestPerception(0, 0, 0, 0);
            var helper = new TestHelper();
            var actions = helper.CreateTestActions();
            var goals = helper.CreateTestGoals();
            var sensors = helper.CreateTestSensors();
            var planner = new SimplePlanner();
            var goalSelector = new GoalSelector();
            var goalUniter = new GoalUniter();
            var utilityProductiveComponent = new UtilityProductiveComponent(actions, goals, planner, goalSelector,
                goalUniter);
            var perceptionUpdater = new FromEachSensorPerceptionUpdater<TestPerception>();
            var stateUpdater = new TestStateUpdater();
            var sensorSystem = new SensorSystem(sensors);
            var runningActionsController = new RunningActionsController();
            var taskManager = new SuccessiveTaskManager(runningActionsController); 
            var target = new UtilityAgent<TestState, TestPerception>(initialState, initialPerception, utilityProductiveComponent,
                perceptionUpdater, stateUpdater, taskManager, sensorSystem);
            var activator = new TestActivator(target);
            activator.Start();
            Assert.IsTrue(World.Value1 == 10);
        }
    }

    internal class TestActivator : IAgentActivator
    {
        public IAgent Agent { get; private set; }
        public TestActivator(IAgent agent)
        {
            Agent = agent;
        }

        public void Start()
        {
            Agent.PerceptionAct();
        }

        public void Stop()
        {
            throw new System.NotImplementedException();
        }
    }
}