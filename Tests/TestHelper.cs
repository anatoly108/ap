﻿using System.Collections.Generic;
using AP.Core.Agent;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.Actuator;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.SensorTools;
using AP.Core.Agent.StateTools;
using Core.Tests.TestAgent.Actions;
using Core.Tests.TestAgent.Actions.ActionFour;
using Core.Tests.TestAgent.Actions.ActionOne;
using Core.Tests.TestAgent.Actions.ActionThree;
using Core.Tests.TestAgent.Actions.ActionTwo;
using Core.Tests.TestAgent.Actuators;
using Core.Tests.TestAgent.Goals.Goal1;
using Core.Tests.TestAgent.Goals.Goal2;
using Core.Tests.TestAgent.Sensors.SensorFour;
using Core.Tests.TestAgent.Sensors.SensorOne;
using Core.Tests.TestAgent.Sensors.SensorThree;
using Core.Tests.TestAgent.Sensors.SensorTwo;
using Core.Tests.TestAgent.State;
using Rhino.Mocks;

namespace Core.Tests
{
    public class TestHelper
    {
        public IAction[] CreateTestActions()
        {
            var consoleActuator = new ConsoleActuator("ConsoleActuator");
            var actuators = new Dictionary<string, Actuator>
            {
                {consoleActuator.Name, consoleActuator}
            };
            var actuatorProvider = new ActuatorProvider(actuators);
            var descriptionToStateConverter = new TestDescriptionToStateConverter();
            var action1Condition = new Action1Condition(descriptionToStateConverter);
            var action1 = new Action1(actuatorProvider, new Action1Result(action1Condition, descriptionToStateConverter), action1Condition, "Action1");
            var action2Condition = new Action2Condition(descriptionToStateConverter);
            var action2 = new Action2(actuatorProvider, new Action2Result(action2Condition, descriptionToStateConverter), action2Condition, "Action2");
            var action3Condition = new Action3Condition(descriptionToStateConverter);
            var action3 = new Action3(actuatorProvider, new Action3Result(action3Condition, descriptionToStateConverter), action3Condition, "Action3");
            var action4Condition = new Action4Condition(descriptionToStateConverter);
            var action4 = new Action4(actuatorProvider, new Action4Result(action4Condition, descriptionToStateConverter), action4Condition, "Action4");

            return new IAction[] { action1, action2, action3, action4 };
        }

        public Goal[] CreateTestGoals()
        {
            var descriptionToStateConverter = new TestDescriptionToStateConverter();
            var goal1Condition = new Goal1Condition(descriptionToStateConverter);
            var goal1Target = new Goal1Target(descriptionToStateConverter);
            var goal1 = new Goal1(goal1Condition, goal1Target, Priority.C);

            var goal2Condition = new Goal2Condition(descriptionToStateConverter);
            var goal2Target = new Goal2Target(descriptionToStateConverter);
            var goal2 = new Goal2(goal2Condition, goal2Target, Priority.C);
            return new Goal[] {goal1, goal2};
        }

        public ISensor[] CreateTestSensors()
        {
            var sensor1 = new Sensor1(new Sensor1Updater());
            var sensor2 = new Sensor2(new Sensor2Updater());
            var sensor3 = new Sensor3(new Sensor3Updater());
            var sensor4 = new Sensor4(new Sensor4Updater());
            return new ISensor[]{sensor1, sensor2, sensor3, sensor4};
        }
    }
}