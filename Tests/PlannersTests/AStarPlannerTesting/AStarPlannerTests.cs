﻿using System.Linq;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.PlaningTools.StateSpace;
using AP.Core.Agent.StateTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks.Constraints;

namespace Core.Tests.PlannersTests.AStarPlannerTesting
{
    [TestClass]
    public class AStarPlannerTests
    {
        [TestMethod]
        public void AStarPlanner_TryPlan_Grid_Test_With_Default_Functions()
        {
            var actions = AStarTestsHelper.CreateTestMovementActions().ToArray();
            var heuristicFunction = new TestHeuristicFunction();
            var testMovementPathCostFunction = AStarTestsHelper.CreateTestMovementPathCostFunction();
            // Default functions
            var isGoalStateFunction = new IsGoalStateFunction();
            var successorFunction = new SuccessorFunction(actions);
            
            var aStarAlgorithm = new AStarAlgorithm(heuristicFunction, isGoalStateFunction, testMovementPathCostFunction, successorFunction);
            var target = new AStarPlanner(aStarAlgorithm);
            var currentState = AStarTestsHelper.CreateTestState(0, 0);
            var targetState = AStarTestsHelper.CreateTestState(3, 2);

            var plan = target.TryPlan(currentState, targetState, actions);

            var actual = plan.ActionsPlan.Count;
            const int expected = 5;

            Assert.IsTrue(expected == actual);
        }
    }
}