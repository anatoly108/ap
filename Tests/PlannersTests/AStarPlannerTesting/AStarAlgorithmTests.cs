﻿using System.Linq;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.PlaningTools.StateSpace;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace Core.Tests.PlannersTests.AStarPlannerTesting
{
    [TestClass]
    public class AStarAlgorithmTests
    {
        [TestMethod]
        public void Algorithm_Execute_Grid_Test()
        {
            var heuristicFunction = new TestHeuristicFunction();
            const int goalX = 2;
            const int goalY = 3;
            const int initialX = 0;
            const int initialY = 0;

            var goalState = AStarTestsHelper.CreateTestState(goalX, goalY);

            var initialState = AStarTestsHelper.CreateTestState(initialX, initialY);

            var isGoalStateFunction = new TestIsGoalStateFunction();
            var pathCostFunction = AStarTestsHelper.CreateTestMovementPathCostFunction();
            var successorFunction = new TestSuccessorFunction();

            var target = new AStarAlgorithm(heuristicFunction, isGoalStateFunction, pathCostFunction, successorFunction);

            var result = target.Execute(initialState, goalState);

            const int expected = 6;
            var actual = result.Count();

            Assert.IsTrue(expected == actual);
        }
    }
}