﻿using AP.Core.Agent.PlaningTools.StateSpace;

namespace Core.Tests.PlannersTests.AStarPlannerTesting
{
    public class TestHeuristicFunction : IHeuristicFunction
    {
        public int Execute(Node currentState, Node targetState)
        {
            var stateCurrent = currentState.StateDescription;
            var stateTarget = targetState.StateDescription;

            var x1 = (int) stateCurrent.Fields["x"].Value;
            var y1 = (int) stateCurrent.Fields["y"].Value;
            var x2 = (int) stateTarget.Fields["x"].Value;
            var y2 = (int) stateTarget.Fields["y"].Value;
            var xMin = x1 < x2 ? x1 : x2;
            var yMin = y1 < y2 ? y1 : y2;
            var xMax = x1 < x2 ? x2 : x1;
            var yMax = y1 < y2 ? y2 : y1;
            var value = (xMax - xMin) + (yMax - yMin);

            return value;
        }
    }
}