﻿using AP.Core.Agent.PlaningTools.StateSpace;

namespace Core.Tests.PlannersTests.AStarPlannerTesting
{
    public class TestIsGoalStateFunction : IIsGoalStateFunction
    {
        public bool Execute(Node state, Node goalState)
        {
            var stateCurrent = state.StateDescription;
            var stateTarget = goalState.StateDescription;

            var x1 = (int)stateCurrent.Fields["x"].Value;
            var y1 = (int)stateCurrent.Fields["y"].Value;
            var x2 = (int)stateTarget.Fields["x"].Value;
            var y2 = (int)stateTarget.Fields["y"].Value;

            var ok = (x1 == x2) && (y1 == y2);
            return ok;
        }
    }
}