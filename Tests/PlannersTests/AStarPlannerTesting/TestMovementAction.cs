﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools;
using Rhino.Mocks;

namespace Core.Tests.PlannersTests.AStarPlannerTesting
{
    public class TestMovementAction : IAction
    {
        public IStateProvider ActionResultStateProvider { get; set; }
        public IStateProvider ActionConditionStateProvider { get; set; }
        public string Name { get; set; }

        public Movement Movement { get; set; }

        public TestMovementAction(Movement movement)
        {
            Movement = movement;
        }

        public void Handle(IState state)
        {
            
        }
    }
}