﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.PlaningTools.StateSpace;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.FieldTools;
using Rhino.Mocks;

namespace Core.Tests.PlannersTests.AStarPlannerTesting
{
    public static class AStarTestsHelper
    {
        public const int GridSize = 10;

        private delegate StateDescription ResultStateProvider(StateDescription currentState);

        public static StateDescription CreateTestState(int x, int y)
        {
            var fields = new Dictionary<string, Field>
            {
                {"x", new Field("x", x)},
                {"y", new Field("y", y)},
            };
            var state = new StateDescription(fields);

            return state;
        }

        public static IAction CreateTestMovementAction(Movement side)
        {
            var action = MockRepository.GenerateStrictMock<IAction>();
            action.Expect(x => x.ActionResultStateProvider).Return(new TestMovementResultProvider(side)).Repeat.Any();
            return action;
        }

        public static IEnumerable<IAction> CreateTestMovementActions()
        {
            var left = CreateTestMovementAction(Movement.Left);
            var right = CreateTestMovementAction(Movement.Right);
            var up = CreateTestMovementAction(Movement.Up);
            var down = CreateTestMovementAction(Movement.Down);

            var list = new List<IAction>
            {
                left,
                right,
                up,
                down
            };

            return list;
        }

        public static IPathCostFunction CreateTestMovementPathCostFunction()
        {
            var pathCostFunction = MockRepository.GenerateStrictMock<IPathCostFunction>();
            pathCostFunction.Expect(x => x.Execute(null, null))
                .IgnoreArguments()
                .Return(1)
                .Repeat.Any();

            return pathCostFunction;
        }
    }

    public class TestMovementResultProvider : IStateProvider
    {
        private readonly Movement _side;

        public TestMovementResultProvider(Movement side)
        {
            _side = side;
        }

        public StateDescription Get(StateDescription currentState)
        {
            var xCoord = (int)currentState.Fields["x"].Value;
            var yCoord = (int)currentState.Fields["y"].Value;

            switch (_side)
            {
                case Movement.Left:
                    xCoord -= 1;
                    break;
                case Movement.Right:
                    xCoord += 1;
                    break;
                case Movement.Up:
                    yCoord += 1;
                    break;
                case Movement.Down:
                    yCoord -= 1;
                    break;
                default:
                    throw new ArgumentOutOfRangeException("side");
            }

            return AStarTestsHelper.CreateTestState(xCoord, yCoord);
        }
    }
}