﻿using System.Collections.Generic;
using AP.Core.Agent.PlaningTools.StateSpace;

namespace Core.Tests.PlannersTests.AStarPlannerTesting
{
    public class TestSuccessorFunction : ISuccessorFunction
    {
        public IEnumerable<Node> Execute(Node currentState)
        {
            var x = (int) currentState.StateDescription.Fields["x"].Value;
            var y = (int) currentState.StateDescription.Fields["y"].Value;

            var result = new List<Node>();

            if (x < AStarTestsHelper.GridSize - 1)
            {
                var state = AStarTestsHelper.CreateTestState(x + 1, y);
                var node = new Node
                {
                    CameFrom = currentState,
                    StateDescription = state
                };
                result.Add(node);
            }
            if (x > 0)
            {
                var state = AStarTestsHelper.CreateTestState(x - 1, y);
                var node = new Node
                {
                    CameFrom = currentState,
                    StateDescription = state
                };
                result.Add(node);
            }
            if (y < AStarTestsHelper.GridSize - 1)
            {
                var state = AStarTestsHelper.CreateTestState(x, y + 1);
                var node = new Node
                {
                    CameFrom = currentState,
                    StateDescription = state
                };
                result.Add(node);
            }
            if (y > 0)
            {
                var state = AStarTestsHelper.CreateTestState(x, y - 1);
                var node = new Node
                {
                    CameFrom = currentState,
                    StateDescription = state
                };
                result.Add(node);
            }

            return result;
        }
    }
}