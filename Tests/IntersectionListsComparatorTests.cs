﻿using AP.Core.Agent.StateTools.FieldTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Core.Tests
{
    [TestClass]
    public class IntersectionListsComparatorTests
    {
        [TestMethod]
        public void IntersectionListsComparator_Compare_String_Array_True_Test()
        {
            var target = new IntersectionListsComparator<string>();
            var arr1 = new[] {"1", "2", "3"};
            var arr2 = new[] {"2", " 4"};
            var field1 = new Field("f1", arr1, typeof(IntersectionListsComparator<string>));
            var field2 = new Field("f2", arr2, typeof(IntersectionListsComparator<string>));
            
            Assert.IsTrue(target.Compare(field1, field2));
        }

        [TestMethod]
        public void IntersectionListsComparator_Compare_String_Array_True_From_Field1_Test()
        {
            var arr1 = new[] { "1", "2", "3" };
            var arr2 = new[] { "2", " 4" };
            var field1 = new Field("f1", arr1, typeof(IntersectionListsComparator<string>));
            var field2 = new Field("f2", arr2, typeof(IntersectionListsComparator<string>));

            Assert.IsTrue(field1.ValueComparator.Compare(field1, field2));
        }
        [TestMethod]
        public void IntersectionListsComparator_Compare_String_Array_True_From_Field2_Test()
        {
            var arr1 = new[] { "1", "2", "3" };
            var arr2 = new[] { "2", " 4" };
            var field1 = new Field("f1", arr1, typeof(IntersectionListsComparator<string>));
            var field2 = new Field("f2", arr2, typeof(IntersectionListsComparator<string>));

            Assert.IsTrue(field2.ValueComparator.Compare(field1, field2));
        }
        [TestMethod]
        public void IntersectionListsComparator_Compare_String_Array_False_From_Field2_Test()
        {
            var arr1 = new[] { "1", "2", "3" };
            var arr2 = new[] { "4" };
            var field1 = new Field("f1", arr1, typeof(IntersectionListsComparator<string>));
            var field2 = new Field("f2", arr2, typeof(IntersectionListsComparator<string>));

            Assert.IsFalse(field2.ValueComparator.Compare(field1, field2));
        }
    }
}