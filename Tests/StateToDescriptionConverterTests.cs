﻿using System.Collections.Generic;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Core.Tests
{
    [TestClass]
    public class StateToDescriptionConverterTests
    {
        [TestMethod]
        public void StateToDescriptionConverter_ToDescription_Test()
        {
            var state = new TestState(10, 20, 30, 3);

            var expectedFields = new Dictionary<string, Field>
            {
                {"Value1", new Field("Value1", 10)},
                {"Value2", new Field("Value2", 20)},
                {"Value3", new Field("Value3", 30)},
                {"Value4", new Field("Value4", 3)},
            };
            var expected = new StateDescription(expectedFields);
            var actual = state.ToDescription();

            Assert.AreEqual(expected, actual);
        }
    }
}