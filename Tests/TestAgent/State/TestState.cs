﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.FieldTools;

namespace Core.Tests.TestAgent.State
{
    public class TestState : IState
    {
        public IHistory History { get; private set; }

        [Field]
        public int? Value1 { get; private set; }

        [Field]
        public int? Value2 { get; private set; }

        [Field]
        public int? Value3 { get; private set; }

        [Field]
        public int? Value4 { get; private set; }

        public TestState(int? value1, int? value2, int? value3, int? value4)
        {
            Value1 = value1;
            Value2 = value2;
            Value3 = value3;
            Value4 = value4;

            History = new History();
        }
    }
}