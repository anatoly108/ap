﻿using AP.Core.Agent.StateTools;
using Core.Tests.TestAgent.Perception;

namespace Core.Tests.TestAgent.State
{
    public class TestStateUpdater : StateUpdater<TestState, TestPerception>
    {
        public override TestState Update(TestState currentState, TestPerception currentPerception)
        {
            return new TestState(currentPerception.Value1, currentPerception.Value2, currentPerception.Value3,
                currentPerception.Value4);
        }
    }
}