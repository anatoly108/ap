using AP.Core.Agent;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.StateTools;

namespace Core.Tests.TestAgent.Goals.Goal4
{
    public class Goal4 : Goal 
    {
        public Goal4(IStateProvider goalConditionStateProvider, IStateProvider goalTargetStateProvider, Priority priority) : base(goalConditionStateProvider, goalTargetStateProvider, priority)
        {
        }
    }
}