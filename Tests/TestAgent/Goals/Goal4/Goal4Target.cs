﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Goals.Goal4
{
    public class Goal4Target : StateProvider<TestState>
    {
        public Goal4Target(DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState currentState)
        {
            return new TestState(null, null, 20, null);
        }
    }
}