﻿using AP.Core.Agent;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.StateTools;

namespace Core.Tests.TestAgent.Goals.Goal5
{
    public class Goal5 : Goal
    {
        public Goal5(IStateProvider goalConditionStateProvider, IStateProvider goalTargetStateProvider, Priority priority) : base(goalConditionStateProvider, goalTargetStateProvider, priority)
        {
        }
    }
}