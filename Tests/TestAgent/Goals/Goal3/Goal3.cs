using AP.Core.Agent;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.StateTools;

namespace Core.Tests.TestAgent.Goals.Goal3
{
    public class Goal3 : Goal 
    {
        public Goal3(IStateProvider goalConditionStateProvider, IStateProvider goalTargetStateProvider, Priority priority) : base(goalConditionStateProvider, goalTargetStateProvider, priority)
        {
        }
    }
}