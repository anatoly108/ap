﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Goals.Goal3
{
    public class Goal3Condition : StateProvider<TestState>
    {
        public Goal3Condition(DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState currentState)
        {
            return new TestState(null, null, 30, null);
        }
    }
}