﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Goals.Goal2
{
    public class Goal2Target : StateProvider<TestState>
    {
        public Goal2Target(DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState currentState)
        {
            return new TestState(10, null, null, null);
        }
    }
}