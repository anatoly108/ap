using AP.Core.Agent;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.StateTools;

namespace Core.Tests.TestAgent.Goals.Goal2
{
    public class Goal2 : Goal 
    {
        public Goal2(IStateProvider goalConditionStateProvider, IStateProvider goalTargetStateProvider, Priority priority) : base(goalConditionStateProvider, goalTargetStateProvider, priority)
        {
        }
    }
}