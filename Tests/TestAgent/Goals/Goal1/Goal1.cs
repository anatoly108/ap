using AP.Core.Agent;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.StateTools;

namespace Core.Tests.TestAgent.Goals.Goal1
{
    public class Goal1 : Goal 
    {
        public Goal1(IStateProvider goalConditionStateProvider, IStateProvider goalTargetStateProvider, Priority priority) : base(goalConditionStateProvider, goalTargetStateProvider, priority)
        {
        }
    }
}