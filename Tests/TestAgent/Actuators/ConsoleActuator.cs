﻿using System;
using AP.Core.Agent.Actuator;

namespace Core.Tests.TestAgent.Actuators
{
    public class ConsoleActuator : Actuator
    {
        public ConsoleActuator(string name) : base(name)
        {
        }

        public override void Handle(string[] args)
        {
            Console.WriteLine(args[0]);
        }
    }
}