﻿namespace Core.Tests.TestAgent.Perception
{
    public class TestPerception : AP.Core.Agent.PerceptionTools.IPerception
    {
        public int Value1 { get; private set; }
        public int Value2 { get; private set; }
        public int Value3 { get; private set; }
        public int Value4 { get; private set; }

        public TestPerception(int value1, int value2, int value3, int value4)
        {
            Value1 = value1;
            Value2 = value2;
            Value3 = value3;
            Value4 = value4;
        }
    }
}