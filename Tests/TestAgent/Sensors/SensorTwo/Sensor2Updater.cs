﻿using AP.Core.Agent.SensorTools;
using Core.Tests.TestAgent.Perception;

namespace Core.Tests.TestAgent.Sensors.SensorTwo
{
    public class Sensor2Updater : PerceptionFromSensorUpdater<TestPerception, int>
    {
        protected override AP.Core.Agent.PerceptionTools.IPerception Update(int handleResult, TestPerception perception)
        {
            return new TestPerception(perception.Value1, handleResult, perception.Value3, perception.Value4);
        }
    }
}