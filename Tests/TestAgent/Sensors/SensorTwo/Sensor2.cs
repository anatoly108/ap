﻿using AP.Core.Agent.SensorTools;

namespace Core.Tests.TestAgent.Sensors.SensorTwo
{
    public class Sensor2 : Sensor<int>
    {
        public Sensor2(IPerceptionFromSensorUpdater perceptionFromSensorUpdater) : base(perceptionFromSensorUpdater)
        {
        }

        public override int Handle()
        {
            return World.Value2;
        }
    }
}