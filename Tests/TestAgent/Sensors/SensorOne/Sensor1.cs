﻿using AP.Core.Agent.SensorTools;

namespace Core.Tests.TestAgent.Sensors.SensorOne
{
    public class Sensor1 : Sensor <int>
    {
        public Sensor1(IPerceptionFromSensorUpdater perceptionFromSensorUpdater) : base(perceptionFromSensorUpdater)
        {
        }

        public override int Handle()
        {
            return World.Value1;
        }
    }
}