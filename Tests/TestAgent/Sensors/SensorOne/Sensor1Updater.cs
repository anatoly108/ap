﻿using AP.Core.Agent.SensorTools;
using Core.Tests.TestAgent.Perception;

namespace Core.Tests.TestAgent.Sensors.SensorOne
{
    public class Sensor1Updater : PerceptionFromSensorUpdater<TestPerception, int>
    {
        protected override AP.Core.Agent.PerceptionTools.IPerception Update(int handleResult, TestPerception perception)
        {
            return new TestPerception(handleResult, perception.Value2, perception.Value3, perception.Value4);
        }
    }
}