﻿using AP.Core.Agent.SensorTools;
using Core.Tests.TestAgent.Perception;

namespace Core.Tests.TestAgent.Sensors.SensorThree
{
    public class Sensor3Updater : PerceptionFromSensorUpdater<TestPerception, int>
    {
        protected override AP.Core.Agent.PerceptionTools.IPerception Update(int handleResult, TestPerception perception)
        {
            return new TestPerception(perception.Value1, perception.Value2, handleResult, perception.Value4);
        }
    }
}