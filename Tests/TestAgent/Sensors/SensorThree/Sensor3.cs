﻿using AP.Core.Agent.SensorTools;

namespace Core.Tests.TestAgent.Sensors.SensorThree
{
    public class Sensor3 : Sensor<int>
    {
        public Sensor3(IPerceptionFromSensorUpdater perceptionFromSensorUpdater) : base(perceptionFromSensorUpdater)
        {
        }

        public override int Handle()
        {
            return World.Value3;
        }
    }
}