﻿using AP.Core.Agent.SensorTools;
using Core.Tests.TestAgent.Perception;

namespace Core.Tests.TestAgent.Sensors.SensorFour
{
    public class Sensor4Updater : PerceptionFromSensorUpdater<TestPerception, int>
    {
        protected override AP.Core.Agent.PerceptionTools.IPerception Update(int handleResult, TestPerception perception)
        {
            return new TestPerception(perception.Value1, perception.Value2, perception.Value3, handleResult);
        }
    }
}