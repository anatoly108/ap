﻿using AP.Core.Agent.SensorTools;

namespace Core.Tests.TestAgent.Sensors.SensorFour
{
    public class Sensor4 : Sensor<int>
    {
        public Sensor4(IPerceptionFromSensorUpdater perceptionFromSensorUpdater) : base(perceptionFromSensorUpdater)
        {
        }

        public override int Handle()
        {
            return World.Value4;
        }
    }
}