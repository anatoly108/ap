﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.Actuator;
using AP.Core.Agent.StateTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionFour
{
    public class Action4 : Action<TestState>
    {
        public Action4(IActuatorProvider actuatorProvider, IStateProvider actionResultStateProvider, IStateProvider actionConditionStateProvider, string name) : base(actuatorProvider, actionResultStateProvider, actionConditionStateProvider, name)
        {
        }

        protected override void Handle(TestState state)
        {
            World.Value4 += 10;
        }
    }
}