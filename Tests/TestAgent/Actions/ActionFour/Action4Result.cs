﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionFour
{
    public class Action4Result : ResultStateProvider<TestState>
    {
        public Action4Result(IStateProvider conditionProvider, DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(conditionProvider, descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState preparedState)
        {
            return new TestState(null, null, null, preparedState.Value4 + 10);
        }
    }
}