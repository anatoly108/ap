﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.Actuator;
using AP.Core.Agent.StateTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionOne
{
    public class Action1 : Action<TestState>
    {
        public Action1(IActuatorProvider actuatorProvider, IStateProvider actionResultStateProvider, IStateProvider actionConditionStateProvider, string name) : base(actuatorProvider, actionResultStateProvider, actionConditionStateProvider, name)
        {
        }

        protected override void Handle(TestState state)
        {
            var actuator = ActuatorProvider.Get("ConsoleActuator");
            World.Value1 += 10;
            World.Value2 -= 10;
        }
    }
}