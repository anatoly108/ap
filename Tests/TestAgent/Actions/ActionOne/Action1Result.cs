﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionOne
{
    public class Action1Result : ResultStateProvider<TestState>
    {
        public Action1Result(IStateProvider conditionProvider, DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(conditionProvider, descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState preparedState)
        {
            return new TestState(preparedState.Value1 + 10, preparedState.Value2 - 10, null, null);
        }
    }
}