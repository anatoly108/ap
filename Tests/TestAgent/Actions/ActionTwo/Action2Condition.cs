﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionTwo
{
    public class Action2Condition : StateProvider<TestState>
    {
        public Action2Condition(DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState currentState)
        {
            return new TestState(null, null, null, null);
        }
    }
}