﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionTwo
{
    public class Action2Result : ResultStateProvider <TestState>
    {
        public Action2Result(IStateProvider conditionProvider, DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(conditionProvider, descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState preparedState)
        {
            // TODO: Может, тут нужно выполнять Handle() этого действия?
            return new TestState(preparedState.Value1 + 10, null, null, null);
        }
    }
}