﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionThree
{
    public class Action3Condition : StateProvider<TestState>
    {
        public Action3Condition(DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState currentState)
        {
            return new TestState(null, null, null, null);
        }
    }
}
