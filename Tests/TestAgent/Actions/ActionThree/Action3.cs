﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.Actuator;
using AP.Core.Agent.StateTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionThree
{
    public class Action3 : Action<TestState>
    {
        public Action3(IActuatorProvider actuatorProvider, IStateProvider actionResultStateProvider, IStateProvider actionConditionStateProvider, string name) : base(actuatorProvider, actionResultStateProvider, actionConditionStateProvider, name)
        {
        }

        protected override void Handle(TestState state)
        {
            World.Value2 -= 10;
        }
    }
}