﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions.ActionThree
{
    public class Action3Result : ResultStateProvider <TestState>
    {
        public Action3Result(IStateProvider conditionProvider, DescriptionToStateConverter<TestState> descriptionToStateConverter) : base(conditionProvider, descriptionToStateConverter)
        {
        }

        protected override TestState Get(TestState preparedState)
        {
            return new TestState(null, preparedState.Value2 - 10, null, null);
        }
    }
}