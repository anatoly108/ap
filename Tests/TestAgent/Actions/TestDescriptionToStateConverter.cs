﻿using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.FieldTools;
using Core.Tests.TestAgent.State;

namespace Core.Tests.TestAgent.Actions
{
    public class TestDescriptionToStateConverter : DescriptionToStateConverter<TestState>
    {
        public override TestState Convert(StateDescription state)
        {
            var val1 = state.GetField("Value1").Value != null ? (int?)state.GetField("Value1").Value : null;
            var val2 = state.GetField("Value2").Value != null ? (int?)state.GetField("Value2").Value : null;
            var val3 = state.GetField("Value3").Value != null ? (int?)state.GetField("Value3").Value : null;
            var val4 = state.GetField("Value4").Value != null ? (int?)state.GetField("Value4").Value : null;

            return new TestState(val1, val2, val3, val4);
        }
    }
}