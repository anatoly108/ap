﻿using System.Collections.Generic;
using AP.Core.Agent.SensorTools;

namespace AP.Core.Agent.PerceptionTools
{
    public interface IPerceptionUpdater<TPerception> where TPerception : IPerception
    {
        TPerception Update(TPerception currentPerception, Dictionary<ISensor, object> handleResults);
    }
}