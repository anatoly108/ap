﻿using System.Collections.Generic;
using AP.Core.Agent.SensorTools;

namespace AP.Core.Agent.PerceptionTools
{
    public class FromEachSensorPerceptionUpdater <TPerception> : IPerceptionUpdater<TPerception> where TPerception : IPerception
    {
        public TPerception Update(TPerception currentPerception, Dictionary<ISensor, object> handleResults)
        {
            var updatedPerception = currentPerception;
            foreach (var resultKeyValuePair in handleResults)
            {
                var sensor = resultKeyValuePair.Key;
                var handleResult = resultKeyValuePair.Value;
                updatedPerception = (TPerception) sensor.PerceptionFromSensorUpdater.Update(handleResult, updatedPerception);
            }
            return updatedPerception;
        }
    }
}