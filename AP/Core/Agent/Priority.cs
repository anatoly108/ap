﻿namespace AP.Core.Agent
{
    public enum Priority
    {
        None,
        F = 4,
        E = 6,
        D = 8,
        C = 10,
        B = 12,
        A = 14
    }
}
