﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.PerceptionTools;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent
{
    public abstract class ProductiveComponent
    {
        public abstract IAction Handle(IPerception currentPerception, StateDescription state, IRunningActionsController runningActionsController);
    }
}