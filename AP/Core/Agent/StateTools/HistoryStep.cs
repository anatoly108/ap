﻿using AP.Core.Agent.ActionTools;

namespace AP.Core.Agent.StateTools
{
    public class HistoryStep
    {
        public IState State { get; private set; }
        public IAction Action { get; private set; }

        public HistoryStep(IState state, IAction action)
        {
            State = state;
            Action = action;
        }
    }
}