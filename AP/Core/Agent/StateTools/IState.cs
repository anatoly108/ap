namespace AP.Core.Agent.StateTools
{
    public interface IState
    {
        IHistory History { get; } 
    }
}