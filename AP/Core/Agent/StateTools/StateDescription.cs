using System;
using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;

namespace AP.Core.Agent.StateTools
{
    sealed public class StateDescription
    {
        public Dictionary<string, Field> Fields  { get; private set; }

        public readonly Dictionary<string, Field> ActiveFields = new Dictionary<string, Field>();

        public StateDescription(Dictionary<string, Field> fields)
        {
            Fields = fields;

            foreach (var field in fields)
            {
                if(field.Value.Value != null)
                    ActiveFields.Add(field.Key, field.Value);
            }
        }

        public Field GetField(string name)
        {
            Field field;
            if (!Fields.TryGetValue(name, out field))
                throw new Exception("�� ������� ���� " + name);

            return field;
        }

        public override bool Equals(object obj)
        {
            var otherDescription = obj as StateDescription;
            if (otherDescription == null)
                return false;

            var result = this.Compare(otherDescription);

            return result;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((ActiveFields != null ? ActiveFields.GetHashCode() : 0)*397) ^ (Fields != null ? Fields.GetHashCode() : 0);
            }
        }
    }
}