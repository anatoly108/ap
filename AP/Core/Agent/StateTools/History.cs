﻿using System;
using System.Collections;
using System.Collections.Generic;
using AP.Core.Agent.ActionTools;

namespace AP.Core.Agent.StateTools
{
    public class History : IHistory
    {
        public const int StepsToRemember = 5;

        private readonly
            Queue<HistoryStep> _steps = new Queue<HistoryStep>();

        public void AddNextStep(IAction action, IState state)
        {
            var step = new HistoryStep(state, action);
            _steps.Enqueue(step);
//            for (var i = 0; i < StepsToRemember - 2; i++)
//            {
//                _steps[i] = _steps[i + 1];
//            }
//            
//            _steps[StepsToRemember - 1] = keyValuePair;
            Console.WriteLine("here");
        }

        public IEnumerable<HistoryStep> GetSteps()
        {
            return _steps;
        }
    }
}