﻿namespace AP.Core.Agent.StateTools
{
    public interface IStateProvider
    {
        StateDescription Get(StateDescription currentState);
    }
}