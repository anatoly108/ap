﻿using System.Collections.Generic;
using AP.Core.Agent.ActionTools;

namespace AP.Core.Agent.StateTools
{
    public interface IHistory
    {
        void AddNextStep(IAction action, IState state);
        IEnumerable<HistoryStep> GetSteps();
    }
}