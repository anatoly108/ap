﻿using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.StateTools.FieldTools;

namespace AP.Core.Agent.StateTools.Extension
{
    public static class StateToDescriptionConverter
    {
        public static StateDescription ToDescription(this IState state)
        {
            var typeOfState = state.GetType();
            var propertiesWithAttribute =
                typeOfState.GetProperties().ToDictionary(x => x, x => x.GetCustomAttributes(typeof(FieldAttribute), true)).Where(x => x.Key.GetCustomAttributes(typeof(FieldAttribute), true).Any());

            var fields = new Dictionary<string, Field>();
            foreach (var keyValuePair in propertiesWithAttribute)
            {
                var property = keyValuePair.Key;
                var attribute = (FieldAttribute) keyValuePair.Value[0];

                var field = new Field(property.Name, property.GetValue(state), attribute.ComparatorType);
                fields.Add(field.Name, field);
            }

            return new StateDescription(fields);
        } 
    }
}