﻿using System.Linq;

namespace AP.Core.Agent.StateTools.Extension
{
    public static class DescriptionExtensions
    {
        // Сравнение полного равенства двух состояний
        public static bool Compare(this StateDescription description1, StateDescription description2)
        {
            return description1.Fields.All(
                x => description2.Fields.Any(
                    y => x.Key == y.Key 
                        && (x.Value == y.Value)));
        }

        // Удовлетворяет ли второе состояние первому (каждое поле не противоречит или равно null)
        public static bool Satisfaction(this StateDescription currentDescription, StateDescription descriptionToCheck)
        {
            return descriptionToCheck.ActiveFields.All(
                x => currentDescription.Fields.Any(y => x.Key == y.Key && x.Value == y.Value)
                );
        }

        // Объединение второго состояния с первым, несмотря на конфликты
        public static StateDescription Unite(this StateDescription current, StateDescription result)
        {
            var resultFields = result.ActiveFields.ToDictionary(activeField => activeField.Key, activeField => activeField.Value);

            foreach (var fieldKeyValuePair in current.Fields)
            {
                if (!resultFields.ContainsKey(fieldKeyValuePair.Key))
                    resultFields.Add(fieldKeyValuePair.Key, fieldKeyValuePair.Value);
            }

            return new StateDescription(resultFields);
        } 
    }
}