﻿using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;

namespace AP.Core.Agent.StateTools
{
    public abstract class StateProvider <TState> : IStateProvider
        where TState : IState
    {
        private readonly DescriptionToStateConverter<TState> _descriptionToStateConverter;

        protected StateProvider(DescriptionToStateConverter<TState> descriptionToStateConverter)
        {
            _descriptionToStateConverter = descriptionToStateConverter;
        }

        public StateDescription Get(StateDescription currentState)
        {
            var state = _descriptionToStateConverter.Convert(currentState);
            var resultState = Get(state);
            return resultState.ToDescription();
        }

        protected abstract TState Get(TState currentState);
    }
}