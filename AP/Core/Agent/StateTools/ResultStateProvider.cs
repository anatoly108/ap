﻿using System;
using System.Collections.Generic;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;

namespace AP.Core.Agent.StateTools
{
    public abstract class ResultStateProvider <TState> : IStateProvider
        where TState : IState
    {
        private readonly IStateProvider _conditionProvider;
        private readonly DescriptionToStateConverter<TState> _descriptionToStateConverter;

        protected ResultStateProvider(IStateProvider conditionProvider, DescriptionToStateConverter<TState> descriptionToStateConverter)
        {
            _conditionProvider = conditionProvider;
            _descriptionToStateConverter = descriptionToStateConverter;
        }

        public StateDescription Get(StateDescription currentState)
        {
            var condition = _conditionProvider.Get(currentState);
            var preparedFields = PrepareFields(currentState, condition);
            var preparedDescription = new StateDescription(preparedFields);
            var preparedState = _descriptionToStateConverter.Convert(preparedDescription);
            var resultState = Get(preparedState);

            return resultState.ToDescription();
        }

        private static Dictionary<string, Field> PrepareFields(StateDescription currentState, StateDescription condition)
        {
            if (condition.GetType() != typeof (StateDescription))
                throw new Exception("Неверный тип описания состояния");

            var fields = condition.Fields;
            var preparedFields = new Dictionary<string, Field>();
            foreach (var conditionFieldKeyValuePair in fields)
            {
                var fieldName = conditionFieldKeyValuePair.Key;
                preparedFields.Add(fieldName, conditionFieldKeyValuePair.Value.Value == null ? currentState.GetField(fieldName) : conditionFieldKeyValuePair.Value);
            }
            return preparedFields;
        }

        protected abstract TState Get(TState preparedState);
    }
}