﻿using AP.Core.Agent.PerceptionTools;

namespace AP.Core.Agent.StateTools
{
    public abstract class StateUpdater <TState, TPerception>
        where TState : IState
        where TPerception : IPerception
    {
        public abstract TState Update(TState currentState, TPerception currentPerception);
    }
}