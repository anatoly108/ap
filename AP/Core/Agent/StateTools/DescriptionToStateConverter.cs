﻿namespace AP.Core.Agent.StateTools
{
    public abstract class DescriptionToStateConverter<TState>
        where TState : IState
    {
        public abstract TState Convert(StateDescription state);
    }
}