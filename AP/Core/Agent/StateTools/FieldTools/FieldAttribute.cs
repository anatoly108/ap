﻿using System;

namespace AP.Core.Agent.StateTools.FieldTools
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class FieldAttribute: Attribute
    {
        public readonly Type ComparatorType;

        public FieldAttribute()
        {
            ComparatorType = typeof (FieldValueComparator);
        }

        public FieldAttribute(Type comparatorType)
        {
            ComparatorType = comparatorType;
        }
    }
}