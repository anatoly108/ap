﻿using System.Collections.Generic;
using System.Linq;

namespace AP.Core.Agent.StateTools.FieldTools
{
    // Компаратор возвращает true, если два множества пересекаются
    public class IntersectionListsComparator <T> : IFieldValueComparator
    {
        public bool Compare(Field field1, Field field2)
        {
            var collection1 = (IEnumerable<T>) field1.Value;
            var collection2 = (IEnumerable<T>) field2.Value;

            return collection1.Intersect(collection2).Any();
        }
    }
}