﻿using System;

namespace AP.Core.Agent.StateTools.FieldTools
{
    public class Field
    {
        public Priority Priority { get; private set; }
        public string Name { get; private set; }
        public object Value { get; private set; }
        public IFieldValueComparator ValueComparator { get; private set; }

        public Field(string name, object value)
        {
            Name = name;
            Value = value;
            Priority = Priority.C;
            ValueComparator = new FieldValueComparator();
        }

        public Field(string name, object value, Type comparatorType)
        {
            Name = name;
            Value = value;
            if (!typeof(IFieldValueComparator).IsAssignableFrom(comparatorType))
                throw new Exception("Неверный тип компаратора");
            Priority = Priority.C;
            ValueComparator = (IFieldValueComparator) Activator.CreateInstance(comparatorType);
        }

        public static bool operator ==(Field field1, Field field2)
        {
            if(field1.Value == null)
                if (field2.Value == null)
                {
                    return true;
                }

            if (field2.Value == null)
                return false;

            return field1.ValueComparator.Compare(field1, field2);
        }

        public static bool operator !=(Field field1, Field field2)
        {
            return !(field1 == field2);
        }
    }
}