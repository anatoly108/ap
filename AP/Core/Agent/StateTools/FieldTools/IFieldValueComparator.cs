﻿namespace AP.Core.Agent.StateTools.FieldTools
{
    public interface IFieldValueComparator
    {
        bool Compare(Field field1, Field field2);
    }
}