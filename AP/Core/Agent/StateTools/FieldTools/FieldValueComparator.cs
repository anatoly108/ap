﻿namespace AP.Core.Agent.StateTools.FieldTools
{
    class FieldValueComparator : IFieldValueComparator
    {
        public bool Compare(Field field1, Field field2)
        {
            return field1.Value.Equals(field2.Value);
        }
    }
}