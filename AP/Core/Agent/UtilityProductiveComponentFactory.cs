﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.PlaningTools;

namespace AP.Core.Agent
{
    public static class UtilityProductiveComponentFactory
    {
        public static UtilityProductiveComponent Create(IAction[] actions, Goal[] goals)
        {
            var planner = new SimplePlanner();
            var goalSelector = new GoalSelector();
            var goalUniter = new GoalUniter();
            return new UtilityProductiveComponent(actions, goals, planner, goalSelector, goalUniter);
        }
    }
}