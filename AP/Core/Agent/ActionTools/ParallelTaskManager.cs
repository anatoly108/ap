using System.Threading;
using System.Threading.Tasks;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.ActionTools
{
    public class ParallelTaskManager : ITaskManager
    {
        public ParallelTaskManager(IRunningActionsController runningActionsController)
        {
            RunningActionsController = runningActionsController;
        }

        public IRunningActionsController RunningActionsController { get; set; }

        public Task NewTask(IAction action, IState currentState, CancellationToken token = new CancellationToken())
        {
            bool contains;
            lock (RunningActionsController)
            {
                contains = RunningActionsController.IsRunning(action);
            }
            if (contains) return null;

            lock (RunningActionsController)
            {
                RunningActionsController.Add(action);
            }

            return Task.Factory.StartNew(() => Action(action, currentState), token);
        }

        private void Action(IAction action, IState currentState)
        {
            action.Handle(currentState);

            lock (RunningActionsController)
            {
                RunningActionsController.Remove(action);
            }
        }
    }
}