﻿using System.Collections.Generic;

namespace AP.Core.Agent.ActionTools
{
    public class RunningActionsController : IRunningActionsController
    {
        // TODO: Что делать с тем, что другие классы получают возможность добавлять и удалять запущенные действия?
        // Запретить добавление и удаление действий для классов, где это не нужно. Например, в IAction.Handle()

        private readonly List<string> _runningActions = new List<string>(); 

        public bool IsRunning(IAction action)
        {
            return _runningActions.Contains(action.Name);
        }

        public void Add(IAction action)
        {
            _runningActions.Add(action.Name);
        }

        public void Remove(IAction action)
        {
            _runningActions.Remove(action.Name);
        }
    }
}