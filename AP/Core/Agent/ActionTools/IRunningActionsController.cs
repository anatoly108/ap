﻿namespace AP.Core.Agent.ActionTools
{
    public interface IRunningActionsController
    {
        bool IsRunning(IAction action);
        void Add(IAction action);
        void Remove(IAction action);
    }
}