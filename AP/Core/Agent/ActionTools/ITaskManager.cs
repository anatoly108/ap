﻿using System.Threading;
using System.Threading.Tasks;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.ActionTools
{
    public interface ITaskManager
    {
        IRunningActionsController RunningActionsController { get; set; }
        Task NewTask(IAction action, IState currentState, CancellationToken token = new CancellationToken());
    }
}