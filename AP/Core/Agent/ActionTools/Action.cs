﻿using System;
using AP.Core.Agent.Actuator;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.ActionTools
{
    public abstract class Action<TState> : IAction
        where TState : IState
    {
        private readonly IActuatorProvider _actuatorProvider;
        public IStateProvider ActionResultStateProvider { get; private set; }
        public IStateProvider ActionConditionStateProvider { get; private set; }
        public string Name { get; private set; }

        protected Action(IActuatorProvider actuatorProvider, IStateProvider actionResultStateProvider, IStateProvider actionConditionStateProvider, string name)
        {
            _actuatorProvider = actuatorProvider;
            ActionResultStateProvider = actionResultStateProvider;
            ActionConditionStateProvider = actionConditionStateProvider;
            Name = name;
        }

        public IActuatorProvider ActuatorProvider
        {
            get { return _actuatorProvider; }
        }

        public void Handle(IState state)
        {
            if (state.GetType() != typeof (TState))
                throw new Exception("Неверный тип состояния");

            var newState = (TState) state;
            Handle(newState);
        }

        protected abstract void Handle(TState state);
    }
}