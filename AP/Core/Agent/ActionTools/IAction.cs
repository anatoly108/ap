﻿using System.Security.Cryptography.X509Certificates;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.ActionTools
{
    public interface IAction
    {
        IStateProvider ActionResultStateProvider { get; }
        IStateProvider ActionConditionStateProvider { get; }
        string Name { get; }
        void Handle(IState state);
    }
}