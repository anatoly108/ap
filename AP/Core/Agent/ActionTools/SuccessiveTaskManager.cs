﻿using System.Threading;
using System.Threading.Tasks;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.ActionTools
{
    public class SuccessiveTaskManager : ITaskManager
    {
        public IRunningActionsController RunningActionsController { get; set; }

        public SuccessiveTaskManager(IRunningActionsController runningActionsController)
        {
            RunningActionsController = runningActionsController;
        }

        // TODO: Тут не за чем возвращать Task, и CancellationToken не нужен
        public Task NewTask(IAction action, IState currentState, CancellationToken token = new CancellationToken())
        {
            RunningActionsController.Add(action);
            action.Handle(currentState);
            RunningActionsController.Remove(action);
            return null;
        }
    }
}