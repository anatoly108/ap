﻿namespace AP.Core.Agent.ActionTools
{
    public interface IActionUtilityCalculator
    {
        float Utility();
    }
}