﻿using System;
using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.FieldTools;

namespace AP.Core.Agent.GoalTools
{
    public class GoalUniter
    {
        public StateDescription Unite(IEnumerable<Goal> goals, StateDescription currentState)
        {
            goals = goals.OrderByDescending(x => x.Priority);
            var goodGoals = new List<Goal>();

            var resultFields = currentState.Fields.ToDictionary(field => field.Key, field => new Field(field.Key, null));

            foreach (var currentGoal in goals)
            {
                var isConflict = goodGoals.Any(goalToCompare => CheckForConflict(currentGoal, goalToCompare, currentState));
                if (!isConflict)
                    goodGoals.Add(currentGoal);
            }

            foreach (var goal in goodGoals)
            {
                foreach (var fieldEntry in goal.GoalTargetStateProvider.Get(currentState).ActiveFields)
                {
                    resultFields[fieldEntry.Key] = fieldEntry.Value;
                }
            }

            return new StateDescription(resultFields);
        }

        public bool CheckForConflict(Goal goal1, Goal goal2, StateDescription currentState)
        {
            var fields = goal2.GoalTargetStateProvider.Get(currentState).Fields;
            var goal1ActiveFields = goal1.GoalTargetStateProvider.Get(currentState).ActiveFields;
            foreach (var field1KeyValuePair in goal1ActiveFields)
            {
                Field field2;
                var field1 = field1KeyValuePair.Value;

                if (!fields.TryGetValue(field1KeyValuePair.Key, out field2))
                    throw new Exception("Не найдено поле " + field1KeyValuePair.Key);

                if (field2.Value == null)
                {
                    continue;
                }
                if (field1 != field2)
                {
                    return true;
                }
            }
            return false;
        }
    }
}