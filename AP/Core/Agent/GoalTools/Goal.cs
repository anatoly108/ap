﻿using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.GoalTools
{
    public abstract class Goal
    {
        public IStateProvider GoalConditionStateProvider { get; private set; }
        public IStateProvider GoalTargetStateProvider { get; private set; }
        public Priority Priority { get; private set; }

        protected Goal(IStateProvider goalConditionStateProvider, IStateProvider goalTargetStateProvider, Priority priority)
        {
            GoalConditionStateProvider = goalConditionStateProvider;
            GoalTargetStateProvider = goalTargetStateProvider;
            Priority = priority;
        }
    }
}