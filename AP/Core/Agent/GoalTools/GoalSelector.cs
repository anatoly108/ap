﻿using System.Collections.Generic;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;

namespace AP.Core.Agent.GoalTools
{
    public class GoalSelector
    {
        public IEnumerable<Goal> Select(StateDescription currentState, IEnumerable<Goal> availableGoals)
        {
            foreach (var availableGoal in availableGoals)
            {
                var descriptionToCheck = availableGoal.GoalConditionStateProvider.Get(currentState);
                if (currentState.Satisfaction(descriptionToCheck))
                {
                    yield return availableGoal;
                }
            }
        }
    }
}