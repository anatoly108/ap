﻿namespace AP.Core.Agent.Actuator
{
    public interface IActuatorProvider
    {
        Actuator Get(string name);
    }
}