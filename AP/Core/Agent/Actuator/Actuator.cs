﻿namespace AP.Core.Agent.Actuator
{
    public abstract class Actuator
    {
        public string Name { get; private set; }

        protected Actuator(string name)
        {
            Name = name;
        }
        // TODO: Ужасно сделано
        public abstract void Handle(string[] args);
    }
}