﻿using System;
using System.Collections.Generic;

namespace AP.Core.Agent.Actuator
{
    public class ActuatorProvider : IActuatorProvider
    {
        private readonly Dictionary<string, Actuator> _actuators;

        public ActuatorProvider(Dictionary<string, Actuator> actuators)
        {
            _actuators = actuators;
        }

        public Actuator Get(string name)
        {
            Actuator actuator;

            if (_actuators.TryGetValue(name, out actuator))
            {
                return actuator;
            }
            else
            {
                throw new Exception("Не найдено актуатора с именем " + name);
            }
        }
    }
}