﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.Components;
using AP.Core.Agent.PerceptionTools;
using AP.Core.Agent.SensorTools;
using AP.Core.Agent.SensorTools.SensorObserver;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;

namespace AP.Core.Agent
{
    public class UtilityAgent<TState, TPerception> : IAgent, ISensorObserver
        where TState : IState
        where TPerception : IPerception
    {
        private readonly SensorSystem _sensorSystem;
        private readonly UtilityProductiveComponent _utilityProductiveComponent;
        private readonly ITaskManager _taskManager;

        private TPerception _perception;
        private readonly IPerceptionUpdater<TPerception> _fromSensorPerceptionUpdater;

        private TState _state;
        private readonly StateUpdater<TState, TPerception> _stateUpdater;

        public UtilityAgent(TState initialState, TPerception initialPerception, UtilityProductiveComponent utilityProductiveComponent,
            IPerceptionUpdater<TPerception> fromSensorPerceptionUpdater, StateUpdater<TState, TPerception> stateUpdater,
            ITaskManager taskManager, SensorSystem sensorSystem, bool registerToSensorSystem = true)
        {
            _utilityProductiveComponent = utilityProductiveComponent;
            _fromSensorPerceptionUpdater = fromSensorPerceptionUpdater;
            _taskManager = taskManager;
            _state = initialState;
            _perception = initialPerception;
            _stateUpdater = stateUpdater;
            _sensorSystem = sensorSystem;

            if(registerToSensorSystem)
                sensorSystem.RegisterObserver(this);
        }

        public void PerceptionAct()
        {
            // Обновляем восприятие
            var handleResults = _sensorSystem.Handle();
            _perception = _fromSensorPerceptionUpdater.Update(_perception, handleResults);
            // Обновляем состояние
            _state = _stateUpdater.Update(_state, _perception);
            // Выбираем действие
            var currentStateDesctiprion = _state.ToDescription();
            // TODO: Прекращать выполнение действия, если построен новый план и требуется другое действие
            var action = _utilityProductiveComponent.Handle(_perception, currentStateDesctiprion,
                _taskManager.RunningActionsController);
            // Выполняем действие
            if (action != null)
                _taskManager.NewTask(action, _state);

            _state.History.AddNextStep(action, null);
        }

        public void Update(ISensorObservable sensor, object value)
        {
            /*
             * Это не самое удачное решение, т.к. значение сенсора может измениться к тому моменту,
             * как мы до него дойдём в PerceptionAct()
             */
            PerceptionAct();
        }
    }
}