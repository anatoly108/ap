﻿using System.Threading;

namespace AP.Core.Agent
{
    public class AgentActivator : IAgentActivator
    {
        private readonly int _perceptionActIntervalMilliseconds;
        public IAgent Agent { get; private set; }
        private bool Stopped { get; set; }

        public AgentActivator(IAgent agent, int perceptionActIntervalMilliseconds = 5)
        {
            Agent = agent;
            _perceptionActIntervalMilliseconds = perceptionActIntervalMilliseconds;
            Stopped = false;
        }

        public void Start()
        {
            while (!Stopped)
            {
                Agent.PerceptionAct();
                Thread.Sleep(_perceptionActIntervalMilliseconds);
            }
        }

        public void Stop()
        {
            Stopped = true;
        }
    }
}