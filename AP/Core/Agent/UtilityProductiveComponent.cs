﻿using System.Dynamic;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.PerceptionTools;
using AP.Core.Agent.PlaningTools;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;

namespace AP.Core.Agent
{
    public class UtilityProductiveComponent : ProductiveComponent
    {
        private IAction[] Actions { get; set; }
        private Goal[] Goals { get; set; }
        private IPlanner Planner { get; set; }
        private GoalSelector GoalSelector { get; set; }
        private GoalUniter GoalUniter { get; set; }

        private StateDescription _previousGoalDescription;
        private IAction _currentAction;
        private Plan _plan;

        public UtilityProductiveComponent(IAction[] actions, Goal[] goals, IPlanner planner, GoalSelector goalSelector,
            GoalUniter goalUniter)
        {
            Actions = actions;
            Goals = goals;
            Planner = planner;
            GoalSelector = goalSelector;
            GoalUniter = goalUniter;
        }

        public override IAction Handle(IPerception perception, StateDescription state, IRunningActionsController runningActionsController)
        {
            // Выбрать цели
            var selectedGoals = GoalSelector.Select(state, Goals);
            // Объединить цели в одно целевое состояние (с учётом приоритетов)
            var finalGoal = GoalUniter.Unite(selectedGoals, state);

            if(state.Satisfaction(finalGoal))
                return null;

            if (_previousGoalDescription != null && finalGoal.Compare(_previousGoalDescription))
            {
                // Цель не изменилась. Но сохраняются ли условия для выполнения плана?

                // Действие выполняется в данный момент и цель не поменялась - просто продолжаем выполнять это действие
                if (_currentAction != null && runningActionsController.IsRunning(_currentAction))
                    return null;

                // Действие плана не выполняется в данный момент - пытаемся выполнить следующее
                var nextAction = _plan.GetNextAction();

                // Если в плане ещё есть действия
                if (nextAction != null)
                {
                    var isActionExutable = state.Satisfaction(nextAction.ActionConditionStateProvider.Get(state));
                    if (isActionExutable)
                    {
                        _currentAction = nextAction;
                        return nextAction;
                    }
                }

                // Составить план, если следующее действие оказалось невыполнимым
                _plan = Planner.TryPlan(state, finalGoal, Actions);
                // Вернуть следующее действие
                _currentAction = _plan.GetNextAction();

                return _currentAction;
            }

            // Цель изменилась - перестраиваем план

            _previousGoalDescription = finalGoal;
            // Составить план
            _plan = Planner.TryPlan(state, finalGoal, Actions);
            // Вернуть следующее действие
            _currentAction = _plan.GetNextAction();

            return _currentAction;
        }
    
    
    }
}