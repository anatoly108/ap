﻿using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools;
using AP.Core.Agent.StateTools.Extension;
using AP.Core.Agent.StateTools.FieldTools;

namespace AP.Core.Agent.PlaningTools
{
    public class SimplePlanner : IPlanner
    {
        public Plan TryPlan(StateDescription currentState, StateDescription targetState, IAction[] actions)
        {
            var resultActionsPlan = new Queue<IAction>();

            var availableActions =
                actions.Where(x => currentState.Satisfaction(x.ActionConditionStateProvider.Get(currentState)));

            if(!availableActions.Any())
                return new Plan(resultActionsPlan);

            var utilities = new Dictionary<IAction, double>();
            var goalActiveFields = targetState.ActiveFields;

            foreach (var action in actions)
            {
                var isOk = true;
                var actionActiveFields = action.ActionResultStateProvider.Get(currentState).ActiveFields;
                var usefulFields = 0;
                var priorityFactor = 1;
                foreach (var goalFieldKeyValuePair in goalActiveFields)
                {
                    Field actionField;
                    var goalField = goalFieldKeyValuePair.Value;

                    if (!actionActiveFields.TryGetValue(goalFieldKeyValuePair.Key, out actionField)) continue;

                    if (goalField == actionField)
                    {
                        usefulFields++;
                        priorityFactor = ((int)goalField.Priority) / 10;
                    }
                    else
                    {
                        isOk = false;
                        break;
                    }
                }

                if(usefulFields == 0)
                    continue;

                if (!isOk)
                    continue;

                var utility = (goalActiveFields.Count/usefulFields)*priorityFactor;
                utilities.Add(action, utility);
            }

            var sortedUtilities = utilities.ToList();
            sortedUtilities.Sort((firstPair, nextPair) => firstPair.Value.CompareTo(nextPair.Value));

            var tmpTargetFields = goalActiveFields.ToDictionary(targetActiveField => targetActiveField.Key,
                targetActiveField => targetActiveField.Value);
            var tmpState = currentState;

            foreach (var utility in sortedUtilities)
            {
                var action = utility.Key;
                var fieldsCount = tmpTargetFields.Count;

                var actionCondition = action.ActionConditionStateProvider.Get(currentState);
                if (!currentState.Satisfaction(actionCondition))
                    continue;

                var actionResult = action.ActionResultStateProvider.Get(currentState);
                var actionActiveFields = actionResult.ActiveFields;
                foreach (var actionActiveField in actionActiveFields)
                {
                    tmpTargetFields.Remove(actionActiveField.Key);
                }

                if (tmpTargetFields.Count == 0)
                {
                    resultActionsPlan.Enqueue(action);
                    return new Plan(resultActionsPlan);
                }

                if (tmpTargetFields.Count == fieldsCount) continue;

                resultActionsPlan.Enqueue(action);
                tmpState = tmpState.Unite(actionResult);
            }

            /**
             * - не работает priorityFactor из-за отсутствия возможности поставить приоритет полю
             * - нужно учитывать, выполнимо ли действие. Проверять сначала только выполнимые действия
             */

//            if (resultActionsPlan.Count == 0)
//            {
//                var planForBestAction = TryPlan(utilities.First().Key.ActionConditionStateProvider.Get(currentState), currentState, actions);
//                resultActionsPlan.AddRange(planForBestAction.ActionsPlan);
//            }

            return new Plan(resultActionsPlan);
        }
    }
}