﻿using System.Collections.Generic;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools.Extension;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public class SuccessorFunction : ISuccessorFunction
    {
        private readonly IEnumerable<IAction> _actions;

        public SuccessorFunction(IEnumerable<IAction> actions)
        {
            _actions = actions;
        }

        public IEnumerable<Node> Execute(Node currentState)
        {
            var neighbors = new List<Node>();

            foreach (var action in _actions)
            {
                var state = action.ActionResultStateProvider.Get(currentState.StateDescription);
                // Этим мы отсекаем состояния такие же как текущее
                if (state.Compare(currentState.StateDescription)) continue;

                var node = new Node
                {
                    Action = action,
                    StateDescription = state
                };
                neighbors.Add(node);
            }
            return neighbors;
        }
    }
}