﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public class Node
    {
        public StateDescription StateDescription { get; set; }
        public IAction Action { get; set; }
        public int HeuristicValue { get; set; } // h(x)
        public int FromBeginingVerticeValue { get; set; } // g(x)
        public int F { get; set; } // f(x) = g(x) + h(x)
        public Node CameFrom { get; set; }
    }
}