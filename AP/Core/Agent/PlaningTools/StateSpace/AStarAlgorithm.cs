﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public class AStarAlgorithm
    {
        private readonly IHeuristicFunction _heuristicFunction;
        private readonly IIsGoalStateFunction _isGoalStateFunction;
        private readonly IPathCostFunction _pathCostFunction;
        private readonly ISuccessorFunction _successorFunction;

        public AStarAlgorithm(IHeuristicFunction heuristicFunction, IIsGoalStateFunction isGoalStateFunction, IPathCostFunction pathCostFunction, ISuccessorFunction successorFunction)
        {
            _heuristicFunction = heuristicFunction;
            _isGoalStateFunction = isGoalStateFunction;
            _pathCostFunction = pathCostFunction;
            _successorFunction = successorFunction;
        }

        public IEnumerable<Node> Execute(StateDescription currentState, StateDescription targetState)
        {
            var start = new Node
            {
                StateDescription = currentState,
                FromBeginingVerticeValue = 0
            };
            var goal = new Node
            {
                StateDescription = targetState,
            };

            start.HeuristicValue = _heuristicFunction.Execute(start, goal);

            var closedSet = new List<Node>();
            var openSet = new List<Node> { start };

            while (openSet.Count > 0)
            {
                var x = openSet.Aggregate((c, d) => c.F < d.F ? c : d);

                if (_isGoalStateFunction.Execute(x, goal))
                {
                    return ReconstructPath(x);
                }

                openSet.Remove(x);
                closedSet.Add(x);

                var neighbors = _successorFunction.Execute(x);

                foreach (var y in neighbors)
                {
                    if (closedSet.Contains(y))
                        continue;

                    var gScore = _pathCostFunction.Execute(x, y);
                    var better = false;

                    if (!openSet.Contains(y))
                    {
                        openSet.Add(y);
                        better = true;
                    }
                    else
                    {
                        if (gScore < y.FromBeginingVerticeValue)
                        {
                            better = true;
                        }
                    }

                    if (!better) continue;

                    y.CameFrom = x;
                    y.FromBeginingVerticeValue = gScore;
                    y.HeuristicValue = _heuristicFunction.Execute(y, goal);
                    y.F = y.FromBeginingVerticeValue + y.HeuristicValue;
                }
            }
            return null; // Не удалось найти путь к конечной вершине
        }

        private IEnumerable<Node> ReconstructPath(Node goal)
        {
            var result = new List<Node>();

            var current = goal;
            while (current != null)
            {
                result.Insert(0, current);
                current = current.CameFrom;
            }

            return result;
        }
    }
}