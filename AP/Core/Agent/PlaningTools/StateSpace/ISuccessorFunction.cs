﻿using System.Collections;
using System.Collections.Generic;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public interface ISuccessorFunction
    {
        IEnumerable<Node> Execute(Node currentState);
    }
}