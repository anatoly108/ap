﻿using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public interface IIsGoalStateFunction
    {
        bool Execute(Node currentState, Node goalState);
    }
}