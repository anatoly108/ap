﻿using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public class AStarPlanner : IPlanner
    {
        private readonly AStarAlgorithm _aStarAlgorithm;

        public AStarPlanner(AStarAlgorithm aStarAlgorithm)
        {
            _aStarAlgorithm = aStarAlgorithm;
        }

        public Plan TryPlan(StateDescription currentState, StateDescription targetState, IAction[] actions)
        {
            var nodes = _aStarAlgorithm.Execute(currentState, targetState);
            
            var queue = new Queue<IAction>();

            // Skip(1), потому что первый Node - текущее состояние, Action коротого равен null
            var planSequence = nodes.Skip(1).Select(x => x.Action); // Взяли все действия
            
            foreach (var action in planSequence)
            {
                queue.Enqueue(action); // Добавляем в очередь
            }

            var plan = new Plan(queue);
            return plan;
        }
    }
}