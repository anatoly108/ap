﻿using System.Windows.Forms;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public interface IHeuristicFunction
    {
        int Execute(Node currentState, Node targetState);
    }
}