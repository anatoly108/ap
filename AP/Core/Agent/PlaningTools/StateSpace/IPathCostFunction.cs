﻿using System.Collections.Generic;
using System.Windows.Forms;
using AP.Core.Agent.ActionTools;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public interface IPathCostFunction
    {
        int Execute(Node currentState, Node nextState);
    }
}