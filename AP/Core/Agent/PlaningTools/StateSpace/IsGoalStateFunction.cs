﻿using AP.Core.Agent.StateTools.Extension;

namespace AP.Core.Agent.PlaningTools.StateSpace
{
    public class IsGoalStateFunction : IIsGoalStateFunction
    {
        public bool Execute(Node currentState, Node goalState)
        {
            return goalState.StateDescription.Satisfaction(currentState.StateDescription);
        }
    }
}