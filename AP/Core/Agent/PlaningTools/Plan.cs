﻿using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.ActionTools;

namespace AP.Core.Agent.PlaningTools
{
    public class Plan
    {
        public Queue<IAction> ActionsPlan { get; private set; }

        public Plan(Queue<IAction> plan)
        {
            ActionsPlan = plan;
        }

        public IAction GetNextAction()
        {
            return !ActionsPlan.Any() ? null : ActionsPlan.Dequeue();
        }
    }
}