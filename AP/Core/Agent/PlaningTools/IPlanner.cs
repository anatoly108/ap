﻿using AP.Core.Agent.ActionTools;
using AP.Core.Agent.GoalTools;
using AP.Core.Agent.StateTools;

namespace AP.Core.Agent.PlaningTools
{
    public interface IPlanner
    {
        Plan TryPlan(StateDescription currentState, StateDescription targetState, IAction[] actions);
    }
}