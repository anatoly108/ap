﻿namespace AP.Core.Agent
{
    public interface IAgent
    {
        void PerceptionAct();
    }
}