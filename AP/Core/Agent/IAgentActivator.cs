﻿namespace AP.Core.Agent
{
    public interface IAgentActivator
    {
        // TODO: Есть проблема, что кто угодно тогда может инициировать PerceptionAct
        IAgent Agent { get; }
        void Start();
        void Stop();
    }
}