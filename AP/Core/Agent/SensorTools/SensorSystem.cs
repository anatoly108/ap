﻿using System.Collections.Generic;
using System.Linq;
using AP.Core.Agent.SensorTools.SensorObserver;

namespace AP.Core.Agent.SensorTools
{
    public class SensorSystem
    {
        private readonly ISensor[] _sensors;

        public SensorSystem(ISensor[] sensors)
        {
            _sensors = sensors;
        }

        public Dictionary<ISensor, object> Handle()
        {
            var results = new Dictionary<ISensor, object>();
            foreach (var sensor in _sensors)
            {
                var handleResult = sensor.Handling();
                results.Add(sensor, handleResult);
            }
            return results;
        }

        public void RegisterObserver(ISensorObserver sensorObserver)
        {
            foreach (var observable in _sensors.OfType<ISensorObservable>())
            {
                observable.RegisterObserver(sensorObserver);
            }
        }
    }
}