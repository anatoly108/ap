﻿namespace AP.Core.Agent.SensorTools.SensorObserver
{
    public interface ISensorObservable
    {
        void RegisterObserver(ISensorObserver sensorObserver);
        void NotifyObservers(ISensorObservable observableSensor, object value);
    }
}