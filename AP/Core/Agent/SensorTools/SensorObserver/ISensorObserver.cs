﻿namespace AP.Core.Agent.SensorTools.SensorObserver
{
    public interface ISensorObserver
    {
        void Update(ISensorObservable sensor, object value);
    }
}