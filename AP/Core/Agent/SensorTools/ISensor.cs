﻿namespace AP.Core.Agent.SensorTools
{
    public interface ISensor
    {
        IPerceptionFromSensorUpdater PerceptionFromSensorUpdater { get; }
        object Handling();
    }
}