using AP.Core.Agent.PerceptionTools;

namespace AP.Core.Agent.SensorTools
{
    public interface IPerceptionFromSensorUpdater
    {
        IPerception Update(object handleResult, IPerception perception);
    }
}