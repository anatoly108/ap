﻿using System.Collections.Generic;
using AP.Core.Agent.SensorTools.SensorObserver;

namespace AP.Core.Agent.SensorTools
{
    // TODO: Может, его стоит сделать наследником от типизированного сенсора? А то отличаются же
    public abstract class ObservableSensor<THandleResult> : Sensor<THandleResult>, ISensorObservable
    {
        private readonly List<ISensorObserver> _observers = new List<ISensorObserver>();

        protected ObservableSensor(IPerceptionFromSensorUpdater perceptionFromSensorUpdater) : base(perceptionFromSensorUpdater)
        {
        }

        public void RegisterObserver(ISensorObserver sensorObserver)
        {
            _observers.Add(sensorObserver);
        }

        public void NotifyObservers(ISensorObservable observableSensor, object value)
        {
            var result = Handling();
            foreach (var sensorObserver in _observers)
            {
                sensorObserver.Update(this, result);
            }
        }
    }
}