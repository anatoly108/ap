using System;
using AP.Core.Agent.PerceptionTools;

namespace AP.Core.Agent.SensorTools
{
    public abstract class PerceptionFromSensorUpdater<TPerception, THandleResult> : IPerceptionFromSensorUpdater
        where TPerception : IPerception
    {

        public IPerception Update(object handleResult, IPerception perception)
        {
            if (handleResult.GetType() != typeof (THandleResult))
                throw new Exception("�������� ���. �������� ���� sensor-sensorUpdater");
            if (perception.GetType() != typeof(TPerception))
                throw new Exception("�������� ��� ����������");

            var result = (THandleResult) handleResult;
            var newPerception = (TPerception) perception;
            return Update(result, newPerception);
        }

        protected abstract IPerception Update(THandleResult handleResult, TPerception perception);
    }
}