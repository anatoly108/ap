﻿namespace AP.Core.Agent.SensorTools
{
    public abstract class Sensor <THandleResult> : ISensor
    {
        public IPerceptionFromSensorUpdater PerceptionFromSensorUpdater { get; private set; }

        protected Sensor(IPerceptionFromSensorUpdater perceptionFromSensorUpdater)
        {
            PerceptionFromSensorUpdater = perceptionFromSensorUpdater;
        }

        public object Handling()
        {
            return Handle();
        }

        public abstract THandleResult Handle();
    }
}