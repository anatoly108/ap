﻿using AP.Core.Agent.Piloting;

namespace AP.Core.Agent.Components
{
    public abstract class TeachingComponent
    {
        private Pilot _pilot;
        public abstract UtilityProductiveComponent Handle();
    }
}